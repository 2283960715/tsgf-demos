
import { IFrameSyncConnect } from "./FrameSyncExecutor";
import { IAfterFrames, IGameSyncFrame, IPlayerInputOperate, Room } from "tsgf-sdk";

export class SDKFrameSyncConnectAdp implements IFrameSyncConnect {


    onAfterFrames: (msg: IAfterFrames) => void = (msg) => { };
    onSyncFrame: (syncFrame: IGameSyncFrame) => void = (msg) => { };
    onRequireSyncState: () => void = () => { };

    sendSyncState(stateData: object, stateFrameIndex: number): void {
        Room.ins.playerSendSyncState(stateData, stateFrameIndex);
    }

    constructor() {
        Room.ins.events.onRecvFrame((frame) => {
            this.onSyncFrame(frame);
        });
        Room.ins.events.onRequirePlayerSyncState(() => {
            this.onRequireSyncState();
        });
    }

}