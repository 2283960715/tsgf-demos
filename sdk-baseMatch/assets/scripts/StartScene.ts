import { _decorator, Component, Node, assetManager, director } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('StartScene')
export class StartScene extends Component {
    start() {
        assetManager.loadBundle('res', (err, bundle) => {
            if (err) return console.error("加载res包失败", err);
            bundle.loadScene('MainScene', function (err, scene) {
                if (err) return console.error("加载res包里的Main场景失败", err);
                director.runScene(scene);
            });
        });
    }

    update(deltaTime: number) {

    }
}

