import { _decorator, Component, Node, Label, Camera } from 'cc';
import { PositionMapToUIComponent } from '../common/ui/PositionMapToUIComponent';
const { ccclass, property } = _decorator;

@ccclass('PlayerNameFlagComponent')
export class PlayerNameFlagComponent extends Component {
    @property({ type: Label, tooltip: "物体名字显示" })
    public NameText!: Label;
    @property({ type: Label, tooltip: "附加信息" })
    public AppendInfo!: Label;
    
    start() {
    }

    update(deltaTime: number) {
        
    }
}

