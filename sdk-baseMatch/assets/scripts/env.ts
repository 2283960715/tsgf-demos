
import { initSDK } from 'tsgf-sdk';

/// #if Miniapp
/// #code import { buildSDKProvider } from 'tsgf-sdk-miniapp';
/// #else
import { buildSDKProvider } from 'tsgf-sdk-browser';
/// #endif


export function initSdkEnv() {
    try {
        initSDK(buildSDKProvider());
    } catch (e) {
        console.log('initSdkEnv:', e);
    }
}
/*

import { HttpClient, WsClient } from "tsrpc-browser";
import { ISDKProvider } from "tsgf-sdk";

export function buildSDKProvider(): ISDKProvider {
    return {
        env: {
            getHttpClient: (proto, options) => {
                return new HttpClient(proto, options);
            },
            getWsClient: (proto, options) => {
                return new WsClient(proto, options);
            },
        }
    };
}


export function initSdkEnv() {
    try {
        initSDK(buildSDKProvider());
    } catch (e) {
        console.log('initSdkEnv:', e.stack);
    }
}
*/
