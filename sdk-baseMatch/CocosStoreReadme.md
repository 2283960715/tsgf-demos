对接 `tsgf-sdk` 的匹配联机示例（含手动创建和加入房间示例）

## 开发环境
- 引擎版本：Cocos Creator 3.5.2
- 编程语言：TypeScript

## 已适配平台

| H5   | 微信小游戏 | Android原生 | iOS原生 | 抖音小游戏 | OPPO小游戏 | vivo小游戏 |
| ---- | ---------| ----------- | -------|-------|-------|-------|
| ✔    | ✔        | ✘           | ✘      |✘      |✘      |✘      |

本资源仅支持测试通过平台，其他平台不做默认支持，请自行适配。

**注意，源码中的多平台支持，是由 [ifdef-creator](https://store.cocos.com/app/detail/3580) 插件实现的选择编译，避免打包不需要平台的npm库！**

## 资源介绍


![效果](https://fengssy.gitee.io/zgame/Games/TSGF/PlayShow.gif?v=1.1.0)



## 功能特点

演示如何使用 `tsgf-sdk` 快速实现一个联机对战游戏！


## 文档教程

- 示例合集官网（**含本示例的使用说明**）：https://gitee.com/fengssy/tsgf-demos
- TSGF 官网：https://gitee.com/fengssy/ts-gameframework

## 联系作者

fengssy@qq.com

### QQ交流群
![QQ群](https://fengssy.gitee.io/zgame/Games/TSGF/TSGF_QQGroupQRCode.png?v=1.1.1)