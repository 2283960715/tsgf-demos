Matching online examples of docking `tsgf-sdk'(including manual room creation and joining examples)

## Development environment

- Engine version: Cocos Creator 3.5.2

- Programming language: TypeScript



## Adapted platform



| H5   | WeChat small game | Android native | iOS native | 抖音小游戏 | OPPO小游戏 | vivo小游戏 |
| ---- | ---------| ----------- | -------|-------|-------|-------|
| ✔    | ✔        | ✘           | ✘      |✘      |✘      |✘      |


This resource only supports the test platform, other platforms do not support the default, please adapt.

**Note that multi-platform support in the source code is provided by [ifdef-creator](https://store.cocos.com/app/detail/3580) Selective compilation of plug-in implementations to avoid packaging NPM libraries that do not require platforms!**

## Resource introduction

![effect](https://fengssy.gitee.io/zgame/Games/TSGF/PlayShow.gif?V=1.1.0)

## Functional features

Demonstrate how to use `tsgf-sdk` to quickly implement an online battle game!


## Documentation tutorial

- the sample collection's official website(**with instructions for using this example**): https://gitee.com/fengssy/tsgf-demos

- the TSGF's official website: https://gitee.com/fengssy/ts-gameframework





## Contact the author

fengssy@qq.com

 

### Tencent QQ Communication Group
![Tencent QQ Communication Group](https://fengssy.gitee.io/zgame/Games/TSGF/TSGF_QQGroupQRCode.png?v=1.1.1)