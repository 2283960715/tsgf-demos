

# 介绍

* 对接 [TSGF (ts-gameframework)](https://gitee.com/fengssy/ts-gameframework) 的客户端示例合集, 示例列表:

    - **sdk-baseMatch**: 对接 `tsgf-sdk` 的简单匹配联机示例（含手动创建房间和加入房间功能）
![效果](https://fengssy.gitee.io/zgame/Games/TSGF/PlayShow.gif?v=1.1.0)
        
        预览地址：

        ![qrcode](https://fengssy.gitee.io/zgame/Games/TSGF/tsgf-sdk-demos-basematch-web-mobileQRCode.png)

* 所有示例均使用的规范:
    - 在 `预览模版` 和 `构建模版` 中配置 `tsgf 的服务器地址`、 `模拟开发者应用站地址` (开发时 `预览模版` 配置本地地址, 部署生产时用 `构建模版` 配置生产或准生产地址)，如：

          globalThis.hallServerUrl = "https://tsgf-hall.iclouden.com/";
          globalThis.demoServerUrl = "https://tsgf-demo-api.iclouden.com/";
    - 入口场景皆为 `./assets/scene/MainScene.scene`

*PS：“模拟开发者应用站地址” 说明：所有对接 `tsgf-sdk` 的开发者，都必须使用自己的玩家的 `openid` 在服务端调用 `TSGF` 的 `api` 获取授权，才能使用 `tsgf-sdk` 连接到服务器，所以才有 “开发者应用站点” 的概念*

# 启动

## 服务端（多种方式，任选其一）

- 使用 `Cocos Creator` 的免费插件 [tsgf-servers-dev for CocosCreator](https://store.cocos.com/app/detail/3910) 启动本地服务端
- 使用的官方平台服务器（源码中用`体验应用`对接官方平台服务器，但流量受限）
- 使用源码自行部署服务端（进入 [TSGF](https://gitee.com/fengssy/ts-gameframework) 查看部署说明）

服务器部署完后，修改预览模版和构建模版中的服务器地址，如下：

    globalThis.hallServerUrl = "http://127.0.0.1:7100/";
    globalThis.demoServerUrl = "http://127.0.0.1:7901/";

## 客户端

1. 示例目录下（如 `./sdk-baseMatch/`）运行命令 `npm i` 初始化npm包（执行一次即可）
2. 导入 `Cocos Creator 3.5.2`
3. 打开场景 `./assets/scene/MainScene.scene`
4. 运行调试（可启动多个预览窗口，用于测试互联）



## QQ交流群
![QQ群](https://fengssy.gitee.io/zgame/Games/TSGF/TSGF_QQGroupQRCode.png?v=1.1.1)